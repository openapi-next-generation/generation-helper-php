<?php

namespace OpenapiNextGeneration\GenerationHelperPhp;

use Composer\Autoload\ClassLoader;

class TargetDirectory
{
    public static bool $createMissingDirectories = false;

    /**
     * Validate if target directory exists
     * return canonical directory path if exists
     *
     * @throws \Exception if directory not exists
     */
    public static function getCanonicalTargetDirectory(string $targetDirectory) : string
    {
        $targetDirectoryCanonical = realpath($targetDirectory);
        if (!is_dir($targetDirectoryCanonical)) {
            if (self::$createMissingDirectories) {
                mkdir($targetDirectory, 0777, true);
                return self::getCanonicalTargetDirectory($targetDirectory);
            } else {
                //no mkdir to avoid generation into unwanted locations because of unintended $targetDirectory
                throw new \Exception('Generator target dir "' . $targetDirectory . '" does not exist. Create it first!');
            }
        }
        return $targetDirectoryCanonical . '/';
    }

    public static function getNamespaceDirectory(string $namespace): string
    {
        $loader = spl_autoload_functions()[0][0];
        if (!$loader instanceof ClassLoader) {
            throw new \Exception('You need to provide the $targetDirectory for EntityBuilder::buildEntities() as you are not using the default composer autoloader!');
        }
        $namespaces = $loader->getPrefixesPsr4();
        return reset($namespaces[$namespace . '\\']);
    }
}
