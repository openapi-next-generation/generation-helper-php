<?php

namespace OpenapiNextGeneration\GenerationHelperPhp;

/**
 * Map type to php type
 */
class TypeMapper
{
    const TYPE_MAP = [
        'integer' => 'int',
        'number' => 'float',
        'boolean' => 'bool'
    ];

    /**
     * @param string $type
     * @return mixed|string
     */
    public static function mapType(string $type)
    {
        return self::TYPE_MAP[$type] ?? $type;
    }
}