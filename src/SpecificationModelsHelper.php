<?php

namespace OpenapiNextGeneration\GenerationHelperPhp;

class SpecificationModelsHelper
{
    public static function readModels(array $specification): array
    {
        $path = static::fetchModelsPath($specification);
        $models = $specification;
        foreach ($path as $segment) {
            $models = $models[$segment];
        }
        return $models;
    }

    public static function writeModels(array $specification, array $models): array
    {
        $path = static::fetchModelsPath($specification);
        $iterator = &$specification;
        foreach ($path as $segment) {
            $iterator = &$iterator[$segment];
        }
        $iterator = $models;
        return $specification;
    }

    public static function fetchModelsPath(array $specification): array
    {
        if (isset($specification['swagger']) && version_compare($specification['swagger'], '3.0') < 0) {
            if (!isset($specification['definitions'])) {
                throw new \Exception('Your specification file has no definitions!');
            }
            $path = ['definitions'];
        } elseif (isset($specification['openapi']) && version_compare($specification['openapi'], '3.0') >= 0) {
            if (!isset($specification['components']) || !isset($specification['components']['schemas'])) {
                throw new \Exception('Your specification file has no components -> schemas!');
            }
            $path = ['components', 'schemas'];
        } else {
            throw new \Exception('Unknown OpenAPI version!');
        }

        return $path;
    }
}